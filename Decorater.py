def deco(get_data):
    print("Outer is running")
    t=get_data()
    def deco2():
            print("Inner is running")
            add=t[0]+t[1]
            return add
    return deco2

@deco
def get_data():
    x = int(input("Enter x value"))
    y = int(input("enter y value"))
    return (x,y)


r=get_data()
#r=l()
print("Type of r = ",type(r))
print(r)